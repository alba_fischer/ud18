import java.sql.Connection;

import dto.Metodos_conexion;
import dto.Peliculas;
import dto.Salas;

public class Exercici04App {

	public static void main(String[] args) {
		// Abrimos conexi�n
		Connection conexion = Metodos_conexion.openConnection();
				
		//Creamos la Database y la tabla peliculas
		Metodos_conexion.createDB("Peliculas_y_salas", conexion);
		Metodos_conexion.createTable("Peliculas_y_salas", "peliculas", conexion);
				
		//Creamos los objetos peliculas y insertamos sus datos en la tabla
		Peliculas peli = new Peliculas(1,"El se�or de los anillos",13);
		Metodos_conexion.insertData("Peliculas_y_salas", "peliculas", peli.getCodigo() , peli.getNombre(),peli.getCalificacionEdad(), conexion);
		Peliculas peli1 = new Peliculas(2,"El corredor del laberinto",16);
		Metodos_conexion.insertData("Peliculas_y_salas", "peliculas", peli1.getCodigo() , peli1.getNombre(),peli1.getCalificacionEdad(), conexion);
		Peliculas peli2 = new Peliculas(3,"Divergente",12);
		Metodos_conexion.insertData("Peliculas_y_salas", "peliculas", peli2.getCodigo() , peli2.getNombre(),peli2.getCalificacionEdad(), conexion);
		Peliculas peli3 = new Peliculas(4,"Insurgente",12);
		Metodos_conexion.insertData("Peliculas_y_salas", "peliculas", peli3.getCodigo() , peli3.getNombre(),peli3.getCalificacionEdad(), conexion);
		Peliculas peli4 = new Peliculas(5,"Leal",12);
		Metodos_conexion.insertData("Peliculas_y_salas", "peliculas", peli4.getCodigo() , peli4.getNombre(),peli4.getCalificacionEdad(), conexion);
		
		//Creamos la tabla salas
		Metodos_conexion.createTable2("Peliculas_y_salas", "salas", conexion);
				
		//Creamos los objetos salas y insertamos sus datos en la tabla
		Salas sala = new Salas(1,"Sala1",peli1.getCodigo());
		Metodos_conexion.insertData2("Peliculas_y_salas", "salas", sala.getCodigo(), sala.getNombre(), sala.getPelicula(), conexion);
		Salas sala1 = new Salas(2,"Sala2",peli2.getCodigo());
		Metodos_conexion.insertData2("Peliculas_y_salas", "salas", sala1.getCodigo(), sala1.getNombre(), sala1.getPelicula(), conexion);
		Salas sala2 = new Salas(3,"Sala3",peli3.getCodigo());
		Metodos_conexion.insertData2("Peliculas_y_salas", "salas", sala2.getCodigo(), sala2.getNombre(), sala2.getPelicula(), conexion);
		Salas sala3 = new Salas(4,"Sala4",peli.getCodigo());
		Metodos_conexion.insertData2("Peliculas_y_salas", "salas", sala3.getCodigo(), sala3.getNombre(), sala3.getPelicula(), conexion);
		Salas sala4 = new Salas(5,"Sala5",peli4.getCodigo());
		Metodos_conexion.insertData2("Peliculas_y_salas", "salas", sala4.getCodigo(), sala4.getNombre(), sala4.getPelicula(), conexion);
				
		//Obtenemos los valores de las dos tablas
		Metodos_conexion.getValues("Peliculas_y_salas", "peliculas", conexion);
		Metodos_conexion.getValues2("Peliculas_y_salas", "salas", conexion);
				
		//Cerramos la conexi�n
		Metodos_conexion.closeConnection(conexion);
	}	

}
