package dto;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Metodos_conexion {
		
		//Metodo que abre la conexion con Mysql
		public static Connection openConnection() {	
			Connection conexion = null;
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				conexion=DriverManager.getConnection("jdbc:mysql://172.20.10.6:3306?useTimezone=true&serverTimezone=UTC","remote","Root123456.");
				System.out.print("Server Connected");
				
			}catch(SQLException | ClassNotFoundException ex  ){
				System.out.print("No se ha podido conectar con la base de datos");
				System.out.println(" ");
				System.out.println(ex.getMessage());	
			}
			return conexion;
			
		}	
		//Metodo que cierra la conexion con Mysql
		public static void closeConnection(Connection conexion) {
			try {
				conexion.close();
				System.out.print("Server Disconnected");
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
				System.out.print("Error al cerrar conexion");			}
			}
			
		//Metodo que nos crea la base de datos
		public static void createDB(String name,Connection conexion) {
			try {
				System.out.println(" ");
				String Query="CREATE DATABASE IF NOT EXISTS "+ name;
				Statement st= conexion.createStatement();
				st.executeUpdate(Query);		
				System.out.println("Se ha creado la DB " + name + " con exito.");
			}catch(SQLException ex) {
				System.out.println(" ");
				System.out.println(ex.getMessage());
				System.out.println("Error al crear la DB.");
			}	
		}	
		//Metodo que crear� la tabla peliculas
		public static void createTable(String db,String name,Connection conexion) {
			try {
				String QueryDB = "USE "+db+";";
				Statement stDB= conexion.createStatement();
				stDB.executeUpdate(QueryDB);
					
				String Query = "CREATE TABLE IF NOT EXISTS " + name + ""
						+ "(codigo INT PRIMARY KEY, nombre NVARCHAR(100), calificacionEdad INT);";
				Statement st= conexion.createStatement();
				st.executeUpdate(Query);
				System.out.println("Tabla creada con exito!");
					
			}catch (SQLException ex){
				System.out.println(ex.getMessage());
				System.out.println("Error al crear la tabla.");
					
				}
				
			}
		//Metodo que crear� la tabla salas
		public static void createTable2(String db,String name,Connection conexion) {
			try {
				String QueryDB = "USE "+db+";";
				Statement stDB = conexion.createStatement();
				stDB.executeUpdate(QueryDB);
					
				String Query = "CREATE TABLE IF NOT EXISTS "+name+""
						+ "(codigo INT PRIMARY KEY, "
						+ "nombre NVARCHAR(100), "
						+ "pelicula INT, "
						+ "FOREIGN KEY (pelicula) REFERENCES peliculas(codigo));";
				Statement st = conexion.createStatement();
				st.executeUpdate(Query);
				System.out.println("Tabla creada con exito!");
					
			}catch (SQLException ex){
				System.out.println(ex.getMessage());
				System.out.println("Error al crear la tabla.");	
				}
				
			}
		
		//Metodo para insertar los datos en la tabla peliculas
		public static void insertData(String db, String table_name, int codigo, String nombre, int calificacionEdad, Connection conexion) {
			try {
				String QueryDB = "USE "+db+";";
				Statement stDB= conexion.createStatement();
				stDB.executeUpdate(QueryDB);
							
				String Query = "INSERT INTO " + table_name + " VALUES(" 
						+ codigo + ", "
						 +"'"+ nombre +"' ,"+ calificacionEdad + " );";
				Statement st = conexion.createStatement();
				st.executeUpdate(Query);
				
				System.out.println("Datos almacenados correctamente");
				
			} catch (SQLException ex ) {
				System.out.println(ex.getMessage());
				System.out.println("Error en el almacenamiento"); 
			}
						
		}
		//Metodo para insertar los datos en la tabla salas
		public static void insertData2(String db, String table_name, int codigo, String nombre, int pelicula, Connection conexion) {
			try {
				String QueryDB = "USE "+db+";";
				Statement stDB= conexion.createStatement();
				stDB.executeUpdate(QueryDB);
								
				String Query = "INSERT INTO " + table_name + " (codigo, nombre, pelicula)VALUES(" 
						+ codigo + ", "
						+"'"+ nombre +"'"+ ","  + pelicula  +");";
				Statement st = conexion.createStatement();
				st.executeUpdate(Query);
					
				System.out.println("Datos almacenados correctamente");
					
			} catch (SQLException ex ) {
				System.out.println(ex.getMessage());
				System.out.println("Error en el almacenamiento"); 
			}
							
		}
			
		//Metodo que obtiene valores de la tabla peliculas
		public static void getValues(String db, String table_name, Connection conexion) {
			try {
				String QueryDB = "USE "+db+";";
				Statement stDB= conexion.createStatement();
				stDB.executeUpdate(QueryDB);
								
				String Query = "SELECT * FROM " + table_name;
				Statement st = conexion.createStatement();
				java.sql.ResultSet resultSet;
				resultSet = st.executeQuery(Query);
				
				System.out.println("TABLA PELICULAS");	
				while (resultSet.next()) {
					System.out.println("Codigo: " +  resultSet.getString("codigo") + " "
							+ "Nombre: " +  resultSet.getString("nombre") + " "
							+ "Calificacion_Edad: " +  resultSet.getString("calificacionEdad") + " "
							);
				}
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
				System.out.println("Error en la adquisicion de datos");
			}
			
		}	
		
		//Metodo que obtiene valores de la tabla salas
		public static void getValues2(String db, String table_name, Connection conexion) {
			try {
				String QueryDB = "USE "+db+";";
				Statement stDB= conexion.createStatement();
				stDB.executeUpdate(QueryDB);
									
				String Query = "SELECT * FROM " + table_name;
				Statement st = conexion.createStatement();
				java.sql.ResultSet resultSet;
				resultSet = st.executeQuery(Query);
				
				System.out.println("TABLA SALAS");
				while (resultSet.next()) {
					System.out.println("Codigo: " +  resultSet.getString("codigo") + " "
							+ "Nombre: " +  resultSet.getString("nombre") + " "
							+ "Pelicula: " +  resultSet.getString("pelicula") + " "
							);
				}
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
				System.out.println("Error en la adquisicion de datos");
			}
				
		}	
}
