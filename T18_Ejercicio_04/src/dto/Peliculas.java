package dto;

public class Peliculas {
	//Atributos
	protected int codigo;
	protected String nombre;
	protected int calificacionEdad;
	
	//Constructor por defecto
	public Peliculas() {
		super();
	}
	
	//Constructor con el codigo, nombre y calificacionEdad
	public Peliculas(int codigo, String nombre, int calificacionEdad) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.calificacionEdad = calificacionEdad;
	}
	
	//Getters
	public int getCodigo() {
		return codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public int getCalificacionEdad() {
		return calificacionEdad;
	}
	
	
	//Setters
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setCalificacionEdad(int calificacionEdad) {
		this.calificacionEdad = calificacionEdad;
	}
	
	
	
}
