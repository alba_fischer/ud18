package dto;

public class Salas {
	//Atributos
	protected int codigo;
	protected String nombre;
	protected int pelicula;
	
	//Constructor por defecto
	public Salas() {	
	}
	
	//Constructor con el codigo, nombre i la pelicula
	public Salas(int codigo, String nombre, int pelicula) {	
		this.codigo = codigo;
		this.nombre = nombre;
		this.pelicula = pelicula;
	}
	
	//Getters
	public int getCodigo() {
		return codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public int getPelicula() {
		return pelicula;
	}
	
	//Setters
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setPelicula(int pelicula) {
		this.pelicula = pelicula;
	}
	
	
}
