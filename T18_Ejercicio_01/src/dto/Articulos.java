package dto;

public class Articulos {
	//Atributos
	protected int codigo;
	protected String nombre;
	protected int precio;
	protected int fabricante;
	
	//CONSTRUCTORES
 
	//Constructor por defecto
	public Articulos() {
	}

	//Constructor con codigo, nombre, precio y fabricante
	public Articulos(int codigo, String nombre, int precio, int fabricante) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.precio = precio;
		this.fabricante = fabricante;
	}

	//Getters
	public int getCodigo() {
		return codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public int getPrecio() {
		return precio;
	}

	public int getFabricante() {
		return fabricante;
	}
	
	//Setters
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setPrecio(int precio) {
		this.precio = precio;
	}

	public void setFabricante(int fabricante) {
		this.fabricante = fabricante;
	}
	

	
	
	
	
}
