package dto;

public class Fabricantes {
	//Atributos
	
	protected int codigo;
	protected String nombre;
	
	
	//CONSTRUCTORES
	
	//Constructor por defecto
	public Fabricantes() {	
	}

	//Constructor con codigo y nombre
	public Fabricantes(int codigo, String nombre) {
		this.codigo = codigo;
		this.nombre = nombre;
	}

	//Getters 
	public int getCodigo() {
		return codigo;
	}

	public String getNombre() {
		return nombre;
	}

	//Setters
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
	
}
