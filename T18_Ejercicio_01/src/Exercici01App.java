import java.sql.Connection;

import dto.Articulos;
import dto.Fabricantes;
import dto.Metodos_conexion;

public class Exercici01App {

	public static void main(String[] args) {
		// Abrimos conexión
		Connection conexion = Metodos_conexion.openConnection();
		
		//Creamos la Database y la tabla fabricantes
		Metodos_conexion.createDB("La_tienda_de_informática", conexion);
		Metodos_conexion.createTable("La_tienda_de_informática", "fabricantes", conexion);
		
		//Creamos los objetos fabricantes y insertamos sus datos en la tabla
		Fabricantes fabricante = new Fabricantes(1,"sonny");
		Metodos_conexion.insertData("La_tienda_de_informática", "fabricantes", fabricante.getCodigo() , fabricante.getNombre(), conexion);
		Fabricantes fabricante1 = new Fabricantes(2,"android");
		Metodos_conexion.insertData("La_tienda_de_informática", "fabricantes", fabricante1.getCodigo() , fabricante1.getNombre(), conexion);
		Fabricantes fabricante2 = new Fabricantes(3,"apple");
		Metodos_conexion.insertData("La_tienda_de_informática", "fabricantes", fabricante2.getCodigo() , fabricante2.getNombre(), conexion);
		Fabricantes fabricante3 = new Fabricantes(4,"samsung");
		Metodos_conexion.insertData("La_tienda_de_informática", "fabricantes", fabricante3.getCodigo() , fabricante3.getNombre(), conexion);
		Fabricantes fabricante4 = new Fabricantes(5,"huawei");
		Metodos_conexion.insertData("La_tienda_de_informática", "fabricantes", fabricante4.getCodigo() , fabricante4.getNombre(), conexion);
		
		//Creamos la tabla articulos
		Metodos_conexion.createTable2("La_tienda_de_informática", "articulos", conexion);
		
		//Creamos los objetos articulos y insertamos sus datos en la tabla
		Articulos articulo = new Articulos(1,"cascos",20, fabricante1.getCodigo());
		Metodos_conexion.insertData2("La_tienda_de_informática", "articulos", articulo.getCodigo(), articulo.getNombre(),articulo.getPrecio(),articulo.getFabricante(), conexion);
		Articulos articulo1 = new Articulos(2,"móbil",100,fabricante3.getCodigo());
		Metodos_conexion.insertData2("La_tienda_de_informática", "articulos", articulo1.getCodigo(), articulo1.getNombre(),articulo1.getPrecio(),articulo1.getFabricante(), conexion);
		Articulos articulo2 = new Articulos(3,"usb",30,fabricante2.getCodigo());
		Metodos_conexion.insertData2("La_tienda_de_informática", "articulos", articulo2.getCodigo(), articulo2.getNombre(),articulo2.getPrecio(),articulo2.getFabricante(), conexion);
		Articulos articulo3 = new Articulos(4,"ordenador",200,fabricante.getCodigo());
		Metodos_conexion.insertData2("La_tienda_de_informática", "articulos", articulo3.getCodigo(), articulo3.getNombre(),articulo3.getPrecio(),articulo3.getFabricante(), conexion);
		Articulos articulo4 = new Articulos(5,"ratón",10,fabricante4.getCodigo());
		Metodos_conexion.insertData2("La_tienda_de_informática", "articulos", articulo4.getCodigo(), articulo4.getNombre(),articulo4.getPrecio(),articulo4.getFabricante(), conexion);
		
		//Obtenemos los valores de las dos tablas
		Metodos_conexion.getValues("La_tienda_de_informática", "fabricantes", conexion);
		Metodos_conexion.getValues2("La_tienda_de_informática", "articulos", conexion);
		
		//Cerramos la conexión
		Metodos_conexion.closeConnection(conexion);
	}

}
