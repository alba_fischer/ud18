package dto;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class grandes_almacenesDB {

	protected String ip;
	protected String port;
	protected String user;
	protected String password;
	
	public grandes_almacenesDB() {
		this.ip = "";
		this.port = "";
		this.user = "";
		this.password = "";
	}
	
	public grandes_almacenesDB(String ip, String port, String user, String password) {
		this.ip = ip;
		this.port = port;
		this.user = user;
		this.password = password;
	}
	
	public Connection connectDB() {
		Connection conexion = null;
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			conexion = DriverManager.getConnection("jdbc:mysql://" + ip + ":" + port +  "?useTimezone=True&serverTimezone=UTC", user, password);
			System.out.println("Servidor conectado");
		} catch (SQLException | ClassNotFoundException e) {
			System.out.println("Ha ocurrido un error al conectar: " + e);
		}
		
		return conexion;
	}
	
	public void disconnectDB(Connection conexion) {
		if(conexion != null) {				
			try {
				conexion.close();
			} catch (SQLException e) {
				System.out.println(e);
			}			
		}
	}
	
	public void createDB() {
		Connection conexion = connectDB();
		
		try {
			String query = "CREATE DATABASE grandes_almacenes;";
			Statement st = conexion.createStatement();
			st.executeUpdate(query);
			
			System.out.println("La base de datos empleados ha sido creada");
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al crear la base de datos empleados: " + e);
		}
		
		disconnectDB(conexion);
	}
	
}
