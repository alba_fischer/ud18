package dto;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class maquinas_registradorasTable extends grandes_almacenesDB {

	public maquinas_registradorasTable() {
		super();
	}
	
	public maquinas_registradorasTable(String ip, String port, String user, String password) {
		super(ip, port, user, password);
	}
	
	public void createTable() {
		Connection conexion = connectDB();
		
		try {
			String queryUse = "USE grandes_almacenes;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String queryCreate = "CREATE TABLE MAQUINAS_REGISTRADORAS ("
					+ "codigo INT AUTO_INCREMENT,"
					+ "piso NVARCHAR(255),"
					+ "PRIMARY KEY(codigo));";
			Statement stCreate = conexion.createStatement();
			stCreate.executeUpdate(queryCreate);
			
			System.out.println("La tabla maquinas registradoras ha sido creada con exito");
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al crear la tabla maquinas registradoras: " + e);
		}
		
		disconnectDB(conexion);
	}
	
	public void insertData(int piso) {
		Connection conexion = connectDB();
		
		try {
			String queryUse = "USE grandes_almacenes;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String queryInsert = "INSERT INTO MAQUINAS_REGISTRADORAS (piso) VALUES (" + piso + ");";
			Statement stInsert = conexion.createStatement();
			stInsert.executeUpdate(queryInsert);
			
			System.out.println("Los datos han sido introducidos con exito");
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al introducir los datos: " + e);
		}
		
		disconnectDB(conexion);
	}
	
	public void getData() {
		Connection conexion = connectDB();
		
		try {
			String queryUse = "USE grandes_almacenes;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String querySelect = "SELECT * FROM MAQUINAS_REGISTRADORAS";
			Statement stSelect = conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = stSelect.executeQuery(querySelect);
			
			while(resultSet.next()) {
				System.out.println("Codigo: " + resultSet.getString("codigo") + " - "
				+ "Piso: " + resultSet.getString("piso")
				);
			}
			
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al obtener la informacion: " + e);
		}
		
		disconnectDB(conexion);
	}
	
	public void deleteRecord(int codigo) {
		Connection conexion = connectDB();
		
		try {
			String queryUse = "USE grandes_almacenes;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String queryDelete = "DELETE FROM MAQUINAS_REGISTRADORAS WHERE codigo = " + codigo;
			Statement stDelete = conexion.createStatement();
			stDelete.executeUpdate(queryDelete);
			
			System.out.println("El registro ha sido eliminado correctamente");
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al eliminar el registro: " + e);
		}
		
		disconnectDB(conexion);
	}
	
}
