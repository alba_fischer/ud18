package dto;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class productosTable extends grandes_almacenesDB {

	public productosTable() {
		super();
	}
	
	public productosTable(String ip, String port, String user, String password) {
		super(ip, port, user, password);
	}
	
	public void createTable() {
		Connection conexion = connectDB();
		
		try {
			String queryUse = "USE grandes_almacenes;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String queryCreate = "CREATE TABLE PRODUCTOS ("
					+ "codigo INT AUTO_INCREMENT,"
					+ "nombre NVARCHAR(100),"
					+ "precio NVARCHAR(255),"
					+ "PRIMARY KEY(codigo));";
			Statement stCreate = conexion.createStatement();
			stCreate.executeUpdate(queryCreate);
			
			System.out.println("La tabla productos ha sido creada con exito");
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al crear la tabla productos: " + e);
		}
		
		disconnectDB(conexion);
	}
	
	public void insertData(String nombre, int precio) {
		Connection conexion = connectDB();
		
		try {
			String queryUse = "USE grandes_almacenes;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String queryInsert = "INSERT INTO PRODUCTOS (nombre, precio) VALUES (" + "'" + nombre + "'" + ", "+ precio + ");";
			Statement stInsert = conexion.createStatement();
			stInsert.executeUpdate(queryInsert);
			
			System.out.println("Los datos han sido introducidos con exito");
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al introducir los datos: " + e);
		}
		
		disconnectDB(conexion);
	}
	
	public void getData() {
		Connection conexion = connectDB();
		
		try {
			String queryUse = "USE grandes_almacenes;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String querySelect = "SELECT * FROM PRODUCTOS";
			Statement stSelect = conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = stSelect.executeQuery(querySelect);
			
			while(resultSet.next()) {
				System.out.println("Codigo: " + resultSet.getString("codigo") + " - "
				+ "Nombre: " + resultSet.getString("nombre") + " - " 
				+ "Precio: " + resultSet.getString("precio")
				);
			}
			
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al obtener la informacion: " + e);
		}
		
		disconnectDB(conexion);
	}
	
	public void deleteRecord(int codigo) {
		Connection conexion = connectDB();
		
		try {
			String queryUse = "USE grandes_almacenes;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String queryDelete = "DELETE FROM PRODUCTOS WHERE codigo = " + codigo;
			Statement stDelete = conexion.createStatement();
			stDelete.executeUpdate(queryDelete);
			
			System.out.println("El registro ha sido eliminado correctamente");
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al eliminar el registro: " + e);
		}
		
		disconnectDB(conexion);
	}
	
}
