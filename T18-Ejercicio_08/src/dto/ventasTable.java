package dto;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class ventasTable extends grandes_almacenesDB {

	public ventasTable() {
		super();
	}
	
	public ventasTable(String ip, String port, String user, String password) {
		super(ip, port, user, password);
	}
	
	public void createTable() {
		Connection conexion = connectDB();
		
		try {
			String queryUse = "USE grandes_almacenes;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String queryCreate = "CREATE TABLE VENTAS ("
					+ "cajero INT,"
					+ "maquina INT,"
					+ "producto INT,"
					+ "PRIMARY KEY(cajero, maquina, producto),"
					+ "FOREIGN KEY (cajero) REFERENCES CAJEROS(codigo),"
					+ "FOREIGN KEY (maquina) REFERENCES MAQUINAS_REGISTRADORAS(codigo),"
					+ "FOREIGN KEY (producto) REFERENCES PRODUCTOS(codigo));";
			Statement stCreate = conexion.createStatement();
			stCreate.executeUpdate(queryCreate);
			
			System.out.println("La tabla ventas ha sido creada con exito");
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al crear la tabla ventas: " + e);
		}
		
		disconnectDB(conexion);
	}
	
	public void insertData(int cajero, int maquina, int producto) {
		Connection conexion = connectDB();
		
		try {
			String queryUse = "USE grandes_almacenes;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String queryInsert = "INSERT INTO VENTAS (cajero, maquina, producto) VALUES (" + cajero + ", " + maquina + ", " + producto + ");";
			Statement stInsert = conexion.createStatement();
			stInsert.executeUpdate(queryInsert);
			
			System.out.println("Los datos han sido introducidos con exito");
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al introducir los datos: " + e);
		}
		
		disconnectDB(conexion);
	}
	
	public void getData() {
		Connection conexion = connectDB();
		
		try {
			String queryUse = "USE grandes_almacenes;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String querySelect = "SELECT * FROM VENTAS";
			Statement stSelect = conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = stSelect.executeQuery(querySelect);
			
			while(resultSet.next()) {
				System.out.println("Cajero: " + resultSet.getString("cajero") + " - "
				+ "Maquina: " + resultSet.getString("maquina") + " - " 
				+ "Producto: " + resultSet.getString("producto")
				);
			}
			
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al obtener la informacion: " + e);
		}
		
		disconnectDB(conexion);
	}
	
	public void deleteRecord(int cajero, int maquina, int producto) {
		Connection conexion = connectDB();
		
		try {
			String queryUse = "USE grandes_almacenes;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String queryDelete = "DELETE FROM VENTAS WHERE cajero = " + cajero + " AND maquina = " + maquina + " AND producto = " + producto + ";";
			Statement stDelete = conexion.createStatement();
			stDelete.executeUpdate(queryDelete);
			
			System.out.println("El registro ha sido eliminado correctamente");
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al eliminar el registro: " + e);
		}
		
		disconnectDB(conexion);
	}
	
}
