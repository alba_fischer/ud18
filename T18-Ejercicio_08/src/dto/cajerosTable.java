package dto;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class cajerosTable extends grandes_almacenesDB {
	
	public cajerosTable() {
		super();
	}
	
	public cajerosTable(String ip, String port, String user, String password) {
		super(ip, port, user, password);
	}
	
	public void createTable() {
		Connection conexion = connectDB();
		
		try {
			String queryUse = "USE grandes_almacenes;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String queryCreate = "CREATE TABLE CAJEROS ("
					+ "codigo INT AUTO_INCREMENT,"
					+ "nom_apels NVARCHAR(255),"
					+ "PRIMARY KEY(codigo));";
			Statement stCreate = conexion.createStatement();
			stCreate.executeUpdate(queryCreate);
			
			System.out.println("La tabla cajeros ha sido creada con exito");
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al crear la tabla cajeros: " + e);
		}
		
		disconnectDB(conexion);
	}
	
	public void insertData(String nom_apels) {
		Connection conexion = connectDB();
		
		try {
			String queryUse = "USE grandes_almacenes;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String queryInsert = "INSERT INTO CAJEROS (nom_apels) VALUES (" + "'" + nom_apels + "'" + ");";
			Statement stInsert = conexion.createStatement();
			stInsert.executeUpdate(queryInsert);
			
			System.out.println("Los datos han sido introducidos con exito");
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al introducir los datos: " + e);
		}
		
		disconnectDB(conexion);
	}
	
	public void getData() {
		Connection conexion = connectDB();
		
		try {
			String queryUse = "USE grandes_almacenes;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String querySelect = "SELECT * FROM CAJEROS";
			Statement stSelect = conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = stSelect.executeQuery(querySelect);
			
			while(resultSet.next()) {
				System.out.println("Codigo: " + resultSet.getString("codigo") + " - "
				+ "Nombre y Apellidos: " + resultSet.getString("nom_apels")
				);
			}
			
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al obtener la informacion: " + e);
		}
		
		disconnectDB(conexion);
	}
	
	public void deleteRecord(int codigo) {
		Connection conexion = connectDB();
		
		try {
			String queryUse = "USE grandes_almacenes;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String queryDelete = "DELETE FROM CAJEROS WHERE codigo = " + codigo;
			Statement stDelete = conexion.createStatement();
			stDelete.executeUpdate(queryDelete);
			
			System.out.println("El registro ha sido eliminado correctamente");
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al eliminar el registro: " + e);
		}
		
		disconnectDB(conexion);
	}
	
}
