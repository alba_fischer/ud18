import dto.*;

public class Ejercicio_08App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		grandes_almacenesDB grandes_almacenesDB = new grandes_almacenesDB("192.168.1.24", "3306", "remote", "a7MWH!%p");
		grandes_almacenesDB.createDB();

		cajerosTable cajeros = new cajerosTable("192.168.1.24", "3306", "remote", "a7MWH!%p");
		cajeros.createTable();
		cajeros.insertData("Josefa Tudela");
		cajeros.insertData("Marc Cayetano");
		cajeros.insertData("Sandra Correa");
		cajeros.insertData("Nazaret Rovira");
		cajeros.insertData("Khalid Galiano");
		cajeros.getData();
		cajeros.deleteRecord(001);
		
		maquinas_registradorasTable maquinasRegistradoras = new maquinas_registradorasTable("192.168.1.24", "3306", "remote", "a7MWH!%p");
		maquinasRegistradoras.createTable();
		maquinasRegistradoras.insertData(001);
		maquinasRegistradoras.insertData(002);
		maquinasRegistradoras.insertData(003);
		maquinasRegistradoras.insertData(004);
		maquinasRegistradoras.insertData(005);
		maquinasRegistradoras.getData();
		maquinasRegistradoras.deleteRecord(001);
		
		productosTable productos = new productosTable("192.168.1.24", "3306", "remote", "a7MWH!%p");
		productos.createTable();
		productos.insertData("Mel�n", 1);
		productos.insertData("Sand�a", 1);
		productos.insertData("Cerezas", 2);
		productos.insertData("Uvas", 3);
		productos.insertData("Manzana", 1);
		productos.getData();
		productos.deleteRecord(001);
		
		ventasTable venta = new ventasTable("192.168.1.24", "3306", "remote", "a7MWH!%p");
		venta.createTable();
		venta.insertData(2, 002, 3);
		venta.insertData(2, 005, 4);
		venta.insertData(3, 005, 2);
		venta.insertData(3, 002, 4);
		venta.insertData(4, 002, 3);
		venta.getData();
		venta.deleteRecord(4, 002, 3);

	}

}
