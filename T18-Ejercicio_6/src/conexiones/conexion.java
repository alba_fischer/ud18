package conexiones;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class conexion {
			
	public static Connection conexion;

	public static void inicio() {
		openConnection();
		createDB();
		createTable();
		insertData();
		getValues();
		deleteRecord();
		closeConnection();
	}
	
	//=====================================================================================
	
	public static void openConnection() {
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			conexion=DriverManager.getConnection("jdbc:mysql://192.168.1.118:3306?useTimezone=true&serverTimezone=UTC", "remote", "Remote123***");
			System.out.println("Conexion completada con �xito");
		}
		catch (SQLException | ClassNotFoundException e) {
			System.out.println(e.getMessage());
			System.out.println("Error al conectar con base de datos");
		}
		
	}
	
	//=====================================================================================
	
	public static void createDB() {
		try {
			String Query = "CREATE DATABASE SUMINISTROS;";
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Se ha creado la base de datos con �xito");
		}
		catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("Error al crear la base de datos");
		}
		
	}
	
	//=====================================================================================
	
	public static void createTable() {
		
		try {
			String Querydb = "USE SUMINISTROS; ";
			Statement stdb= conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "CREATE TABLE PIEZAS (Codigo INT, Nombre VARCHAR(100), PRIMARY KEY (Codigo)) ";
			String Query1 = "CREATE TABLE PROVEEDORES (Id Char(4), Nombre VARCHAR(100), PRIMARY KEY (Id)) ";
			String Query2 = "CREATE TABLE SUMINISTRA (CodigoPieza INT , IdProveedor char(4), precio INT, PRIMARY KEY(CodigoPieza, IdProveedor), FOREIGN KEY (CodigoPieza) REFERENCES PIEZAS (Codigo), FOREIGN KEY (IdProveedor) REFERENCES PROVEEDORES (Id))";
			Statement st= conexion.createStatement();
			st.executeUpdate(Query);
			st.executeUpdate(Query1);
			st.executeUpdate(Query2);
			System.out.println("Tabla 1 creada con �xito");
			
			
		}
		catch (SQLException e){
			System.out.println(e.getMessage());
			System.out.println("Error al crear la tabla");
		}
		
	}
	
	//=====================================================================================
	
	public static void insertData() {
		
		try {
			
			String Querydb = "USE SUMINISTROS; ";
			Statement stdb= conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			//insert into PIEZAS
			String Query = "INSERT INTO PIEZAS VALUES (1,  \"Rosca\"), (2,  \"Muelle\"), ( 3,  \"Taladro\"), ( 4,  \"Destronillador\"), ( 5,  \"Tornillo\");";
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Datos almacenados correctamente");
		
			//insert into PROVEEDORES
			String Query1 = "INSERT INTO PROVEEDORES VALUES (0001, \"Cocinas Mario\"), (0002, \"Ferreteria Marti\"), (0003, \"Constructores jose\"), (0004, \"Electronica Paco\"), (0005, \"Mat\");";
			Statement st1 = conexion.createStatement();
			st1.executeUpdate(Query1);
			System.out.println("Datos almacenados correctamente");
			
			String Query2 = "INSERT INTO SUMINISTRA VALUES (1, 0001, 12), (2, 0002, 45), (3, 0003, 20), (4, 0004, 27), (5, 0005, 60);";
			Statement st2 = conexion.createStatement();
			st2.executeUpdate(Query2);
			System.out.println("Datos almacenados correctamente");
			
		}
		catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("Error al insertar datos");
		}
		
	}
	
	//=====================================================================================

	public static void getValues() {
		

			try {
				
				String Query = "USE SUMINISTROS; ";
				Statement st= conexion.createStatement();
				st.executeUpdate(Query);
				
				String Query1 = "SELECT * FROM PIEZAS";
				Statement st1 = conexion.createStatement();
				java.sql.ResultSet resultSet1;
				resultSet1 = st1.executeQuery(Query1);
				
				while(resultSet1.next()) { 
					System.out.println("Codigo: " + resultSet1.getString("Codigo") + " Nombre: " + resultSet1.getString("Nombre"));
				}

				String Query2 = "SELECT * FROM PROVEEDORES";
				Statement st2 = conexion.createStatement();
				java.sql.ResultSet resultSet2;
				resultSet2 = st2.executeQuery(Query2);
				
				while(resultSet2.next()) { 
					System.out.println("Id: " + resultSet2.getString("Id") + " Nombre: " + resultSet2.getString("Nombre"));
				}
		
				String Query3 = "SELECT * FROM SUMINISTRA";
				Statement st3 = conexion.createStatement();
				java.sql.ResultSet resultSet3;
				resultSet3 = st3.executeQuery(Query3);
				
				while(resultSet3.next()) { 
					System.out.println("CodigoPieza: " + resultSet3.getString("Codigopieza") + " IdProveedor: " + resultSet3.getString("IdProveedor") + " Precio: " + resultSet3.getString("Precio"));
				}
				
				
				
			}
			catch (SQLException e) {
				System.out.println(e.getMessage());
				System.out.println("Error al mostrar datos");
			}	
		

	}
	
	
	//=====================================================================================
	
	//FALLO???
	
	
	public static void deleteRecord() {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("De que tabla quieres eliminar?");
		String tabla = scan.next();
		
		if (tabla.equalsIgnoreCase("PIEZAS")) {
			
			System.out.println("Codigo que quieres eliminar");
			int numero = scan.nextInt();
		
			try {
				String Query = "DELETE FROM PIEZAS WHERE Codigo = "+ numero ; 
				Statement stDelete = conexion.createStatement();
				stDelete.executeUpdate(Query);
				System.out.println("Registro eliminado");
			}
			catch (SQLException e) {
				System.out.println(e.getMessage());
				System.out.println("Error al borrar el especificado");
			}
		}
		else if (tabla.equalsIgnoreCase("PROVEEDORES")) {
			System.out.println("Id proveedor que quieres eliminar");
			int numero = scan.nextInt();
		
			try {
				String Query = "DELETE FROM PROVEEDORES WHERE Id = " + numero; 
				Statement stDelete = conexion.createStatement();
				stDelete.executeUpdate(Query);
				System.out.println("Registro eliminado");
			}
			catch (SQLException e) {
				System.out.println(e.getMessage());
				System.out.println("Error al borrar el especificado");
			}
		}
		else if (tabla.equalsIgnoreCase("SUMINISTRA")) {
			System.out.println("Codigo Pieza que quieres eliminar");
			int numero = scan.nextInt();
		
			try {
				String Query = "DELETE FROM SUMINISTRA WHERE Codigopieza = " + numero; 
				Statement stDelete = conexion.createStatement();
				stDelete.executeUpdate(Query);
				System.out.println("Registro eliminado");
			}
			catch (SQLException e) {
				System.out.println(e.getMessage());
				System.out.println("Error al borrar el especificado");
			}
		}
		else {
			System.out.println("Nombre de tabla incorrecto");
		}
		
		scan.close();

	}
	
	//=====================================================================================
	
	
	public static void closeConnection() {
		try {
			conexion.close();
			System.out.println("Conexion finalizada");
		}
		catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("Error al cerrar conexion");
		}
		
	}
	
}

