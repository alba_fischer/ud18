package dto;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Metodos_conexion {
	//Metodo que abre la conexion con Mysql
		public static Connection openConnection() {	
			Connection conexion = null;
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				conexion=DriverManager.getConnection("jdbc:mysql://172.20.10.6:3306?useTimezone=true&serverTimezone=UTC","remote","Root123456.");
				System.out.print("Server Connected");
				
			}catch(SQLException | ClassNotFoundException ex  ){
				System.out.print("No se ha podido conectar con la base de datos");
				System.out.println(" ");
				System.out.println(ex.getMessage());	
			}
			return conexion;
			
		}	
		//Metodo que cierra la conexion con Mysql
		public static void closeConnection(Connection conexion) {
			try {
				conexion.close();
				System.out.print("Server Disconnected");
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
				System.out.print("Error al cerrar conexion");			}
			}
			
		//Metodo que nos crea la base de datos
		public static void createDB(String name,Connection conexion) {
			try {
				System.out.println(" ");
				String Query="CREATE DATABASE IF NOT EXISTS "+ name;
				Statement st= conexion.createStatement();
				st.executeUpdate(Query);		
				System.out.println("Se ha creado la DB " + name + " con exito.");
			}catch(SQLException ex) {
				System.out.println(" ");
				System.out.println(ex.getMessage());
				System.out.println("Error al crear la DB.");
			}	
		}	
		//Metodo que crear� la tabla cientificos
		public static void createTable(String db,String name,Connection conexion) {
			try {
				String QueryDB = "USE "+db+";";
				Statement stDB= conexion.createStatement();
				stDB.executeUpdate(QueryDB);
					
				String Query = "CREATE TABLE IF NOT EXISTS " + name + ""
						+ "(dni VARCHAR(8) PRIMARY KEY, nomApels NVARCHAR(255));";
				Statement st= conexion.createStatement();
				st.executeUpdate(Query);
				System.out.println("Tabla creada con exito!");
					
			}catch (SQLException ex){
				System.out.println(ex.getMessage());
				System.out.println("Error al crear la tabla.");
					
				}
				
			}
		//Metodo que crear� la tabla proyectos
		public static void createTable1(String db,String name,Connection conexion) {
				try {
					String QueryDB = "USE "+db+";";
					Statement stDB= conexion.createStatement();
					stDB.executeUpdate(QueryDB);
							
					String Query = "CREATE TABLE IF NOT EXISTS " + name + ""
							+ "(id CHAR(4) PRIMARY KEY, nombre NVARCHAR(255), horas INT);";
					Statement st= conexion.createStatement();
					st.executeUpdate(Query);
					System.out.println("Tabla creada con exito!");
							
				}catch (SQLException ex){
					System.out.println(ex.getMessage());
					System.out.println("Error al crear la tabla.");
							
					}
						
				}
		//Metodo que crear� la tabla asignado_a
		public static void createTable2(String db,String name,Connection conexion) {
			try {
				String QueryDB = "USE "+db+";";
				Statement stDB = conexion.createStatement();
				stDB.executeUpdate(QueryDB);
					
				String Query = "CREATE TABLE IF NOT EXISTS "+name+""
						+ "(cientifico VARCHAR(8), "
						+ "proyecto CHAR(4), "
						+ "FOREIGN KEY (cientifico) REFERENCES cientificos(dni),"
						+ "FOREIGN KEY (proyecto) REFERENCES proyectos(id));";
				Statement st = conexion.createStatement();
				st.executeUpdate(Query);
				System.out.println("Tabla creada con exito!");
					
			}catch (SQLException ex){
				System.out.println(ex.getMessage());
				System.out.println("Error al crear la tabla.");	
				}
				
			}
		
		//Metodo para insertar los datos en la tabla cientificos
		public static void insertData(String db, String table_name, String dni, String nomApels, Connection conexion) {
			try {
				String QueryDB = "USE "+db+";";
				Statement stDB= conexion.createStatement();
				stDB.executeUpdate(QueryDB);
							
				String Query = "INSERT INTO " + table_name + " VALUES(" 
						+"'" + dni + "', "
						 +"'"+ nomApels +"' );";
				Statement st = conexion.createStatement();
				st.executeUpdate(Query);
				
				System.out.println("Datos almacenados correctamente");
				
			} catch (SQLException ex ) {
				System.out.println(ex.getMessage());
				System.out.println("Error en el almacenamiento"); 
			}
						
		}
		//Metodo para insertar los datos en la tabla proyectos
		public static void insertData1(String db, String table_name, char id, String nombre,int horas, Connection conexion) {
			try {
				String QueryDB = "USE "+db+";";
				Statement stDB= conexion.createStatement();
				stDB.executeUpdate(QueryDB);
									
				String Query = "INSERT INTO " + table_name + " VALUES(" 
						+ id + ", "
						+"'"+ nombre +"' ," + horas + " );";
				Statement st = conexion.createStatement();
				st.executeUpdate(Query);
						
				System.out.println("Datos almacenados correctamente");
						
				} catch (SQLException ex ) {
					System.out.println(ex.getMessage());
					System.out.println("Error en el almacenamiento"); 
				}
								
		}
		//Metodo para insertar los datos en la tabla asignado_a
		public static void insertData2(String db, String table_name, String cientifico, char proyecto, Connection conexion) {
			try {
				String QueryDB = "USE "+db+";";
				Statement stDB= conexion.createStatement();
				stDB.executeUpdate(QueryDB);
								
				String Query = "INSERT INTO " + table_name + " (cientifico,proyecto)VALUES(" 
						+"'"+ cientifico +"'"+ "," + proyecto + ");";
				Statement st = conexion.createStatement();
				st.executeUpdate(Query);
					
				System.out.println("Datos almacenados correctamente");
					
			} catch (SQLException ex ) {
				System.out.println(ex.getMessage());
				System.out.println("Error en el almacenamiento"); 
			}
							
		}
			
		//Metodo que obtiene valores de la tabla cientificos
		public static void getValues(String db, String table_name, Connection conexion) {
			try {
				String QueryDB = "USE "+db+";";
				Statement stDB= conexion.createStatement();
				stDB.executeUpdate(QueryDB);
								
				String Query = "SELECT * FROM " + table_name;
				Statement st = conexion.createStatement();
				java.sql.ResultSet resultSet;
				resultSet = st.executeQuery(Query);
				
				System.out.println("TABLA CIENT�FICOS");	
				while (resultSet.next()) {
					System.out.println("DNI: " +  resultSet.getString("dni") + " "
							+ "Nombre_Apellidos: " +  resultSet.getString("nomApels") + " "
							);
				}
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
				System.out.println("Error en la adquisicion de datos");
			}
			
		}
		//Metodo que obtiene valores de la tabla proyectos
		public static void getValues1(String db, String table_name, Connection conexion) {
			try {
				String QueryDB = "USE "+db+";";
				Statement stDB= conexion.createStatement();
				stDB.executeUpdate(QueryDB);
										
				String Query = "SELECT * FROM " + table_name;
				Statement st = conexion.createStatement();
				java.sql.ResultSet resultSet;
				resultSet = st.executeQuery(Query);
						
				System.out.println("TABLA PROYECTOS");	
				while (resultSet.next()) {
					System.out.println("id: " +  resultSet.getString("id") + " "
							+ "Nombre: " +  resultSet.getString("nombre") + " "
							+ "Horas: " +  resultSet.getString("horas") + " "
							);
						}
					} catch (SQLException ex) {
						System.out.println(ex.getMessage());
						System.out.println("Error en la adquisicion de datos");
					}
					
				}
		
		//Metodo que obtiene valores de la tabla asignado_a
		public static void getValues2(String db, String table_name, Connection conexion) {
			try {
				String QueryDB = "USE "+db+";";
				Statement stDB= conexion.createStatement();
				stDB.executeUpdate(QueryDB);
									
				String Query = "SELECT * FROM " + table_name;
				Statement st = conexion.createStatement();
				java.sql.ResultSet resultSet;
				resultSet = st.executeQuery(Query);
				
				System.out.println("TABLA ASIGNADO_A");
				while (resultSet.next()) {
					System.out.println("Cientifico: " +  resultSet.getString("cientifico") + " "
							+ "Proyecto: " +  resultSet.getString("proyecto") + " "
							);
				}
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
				System.out.println("Error en la adquisicion de datos");
			}
				
		}	
}
