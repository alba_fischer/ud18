package dto;

public class Cientificos {
	//Atributos
	protected String dni;
	protected String nomApels;
	
	//Constructor por defecto
	public Cientificos() {
	}
	
	//Constructor con dni y nomApels
	public Cientificos(String dni, String nomApels) {
		this.dni = dni;
		this.nomApels = nomApels;
	}
	
	//Getters
	public String getDni() {
		return dni;
	}
	public String getNomApels() {
		return nomApels;
	}
	
	//Setters
	public void setDni(String dni) {
		this.dni = dni;
	}
	public void setNomApels(String nomApels) {
		this.nomApels = nomApels;
	}
	
	
}
