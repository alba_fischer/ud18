package dto;

public class Proyecto {
	//Atributos
	protected char id;
	protected String nombre;
	protected int horas;
	
	//Constructor por defecto
	public Proyecto() {
	}
	
	//Constructor con id, nombre y horas
	public Proyecto(char id, String nombre, int horas) {
		this.id = id;
		this.nombre = nombre;
		this.horas = horas;
	}

	//Getters
	public char getId() {
		return id;
	}
	public String getNombre() {
		return nombre;
	}
	public int getHoras() {
		return horas;
	}

	//Setters
	public void setId(char id) {
		this.id = id;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setHoras(int horas) {
		this.horas = horas;
	}
	
	
	
}
