import java.sql.Connection;

import dto.Cientificos;
import dto.Metodos_conexion;
import dto.Proyecto;

public class Exercici07App {

	public static void main(String[] args) {
		// Abrimos conexión
		Connection conexion = Metodos_conexion.openConnection();
				
		//Creamos la Database y la tabla cientificos
		Metodos_conexion.createDB("LosCientificos", conexion);
		Metodos_conexion.createTable("LosCientificos", "cientificos", conexion);
		
		//Creamos los objetos cientificos y insertamos sus datos en la tabla
		Cientificos cientifico = new Cientificos("45896321","Albert Einstein");
		Metodos_conexion.insertData("LosCientificos", "cientificos", cientifico.getDni() , cientifico.getNomApels(), conexion);
		Cientificos cientifico1 = new Cientificos("75986231","Isaac Newton");
		Metodos_conexion.insertData("LosCientificos", "cientificos", cientifico1.getDni() , cientifico1.getNomApels(), conexion);
		Cientificos cientifico2 = new Cientificos("26598314","Marie Curie");
		Metodos_conexion.insertData("LosCientificos", "cientificos", cientifico2.getDni() , cientifico2.getNomApels(), conexion);
		Cientificos cientifico3 = new Cientificos("33652168","Stephen Hawking");
		Metodos_conexion.insertData("LosCientificos", "cientificos", cientifico3.getDni() , cientifico3.getNomApels(), conexion);
		Cientificos cientifico4 = new Cientificos("79685321","Charles Darwin");
		Metodos_conexion.insertData("LosCientificos", "cientificos", cientifico4.getDni() , cientifico4.getNomApels(), conexion);

		//Creamos la tabla proyectos
		Metodos_conexion.createTable1("LosCientificos", "proyectos", conexion);
		
		//Creamos los objetos proyecto y insertamos sus datos en la tabla
		Proyecto proyecto = new Proyecto('1',"Gravedad",200);
		Metodos_conexion.insertData1("LosCientificos", "proyectos", proyecto.getId() , proyecto.getNombre(), proyecto.getHoras(),conexion);
		Proyecto proyecto1 = new Proyecto('2',"Polímeros",150);
		Metodos_conexion.insertData1("LosCientificos", "proyectos", proyecto1.getId() , proyecto1.getNombre(), proyecto1.getHoras(),conexion);
		Proyecto proyecto2 = new Proyecto('3',"Electricidad",250);
		Metodos_conexion.insertData1("LosCientificos", "proyectos", proyecto2.getId() , proyecto2.getNombre(), proyecto2.getHoras(),conexion);
		Proyecto proyecto3 = new Proyecto('4',"Relatividad",300);
		Metodos_conexion.insertData1("LosCientificos", "proyectos", proyecto3.getId() , proyecto3.getNombre(), proyecto3.getHoras(),conexion);
		Proyecto proyecto4 = new Proyecto('5',"Evolución",210);
		Metodos_conexion.insertData1("LosCientificos", "proyectos", proyecto4.getId() , proyecto4.getNombre(), proyecto4.getHoras(),conexion);
		
		//Creamos la tabla asignado_a
		Metodos_conexion.createTable2("LosCientificos", "asignado_a", conexion);
		
		//Insertamos sus datos en la tabla
		Metodos_conexion.insertData2("LosCientificos", "asignado_a", cientifico.getDni(),proyecto2.getId(),conexion);
		Metodos_conexion.insertData2("LosCientificos", "asignado_a", cientifico1.getDni(),proyecto.getId(),conexion);
		Metodos_conexion.insertData2("LosCientificos", "asignado_a", cientifico2.getDni(),proyecto1.getId(),conexion);
		Metodos_conexion.insertData2("LosCientificos", "asignado_a", cientifico3.getDni(),proyecto3.getId(),conexion);
		Metodos_conexion.insertData2("LosCientificos", "asignado_a", cientifico4.getDni(),proyecto4.getId(),conexion);
		
		//Obtenemos los valores de las dos tablas
		Metodos_conexion.getValues("LosCientificos", "cientificos", conexion);
		Metodos_conexion.getValues1("LosCientificos", "proyectos", conexion);
		Metodos_conexion.getValues2("LosCientificos", "asignado_a", conexion);
				
		//Cerramos la conexión
		Metodos_conexion.closeConnection(conexion);
	}

}
