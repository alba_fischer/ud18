package conexiones;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class conexion {
			
	public static Connection conexion;

	public static void inicio() {
		openConnection();
		createDB();
		createTable();
		insertData();
		getValues();
		deleteRecord();
		closeConnection();
	}
	
	//=====================================================================================
	
	public static void openConnection() {
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			conexion=DriverManager.getConnection("jdbc:mysql://192.168.1.118:3306?useTimezone=true&serverTimezone=UTC", "remote", "Remote123***");
			System.out.println("Conexion completada con �xito");
		}
		catch (SQLException | ClassNotFoundException e) {
			System.out.println(e.getMessage());
			System.out.println("Error al conectar con base de datos");
		}
		
	}
	
	//=====================================================================================
	
	public static void createDB() {
		try {
			String Query = "CREATE DATABASE EJ09;";
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Se ha creado la base de datos con �xito");
		}
		catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("Error al crear la base de datos");
		}
		
	}
	
	//=====================================================================================
	
	public static void createTable() {
		
		try {
			String Querydb = "USE EJ09; ";
			Statement stdb= conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "CREATE TABLE FACULTAD (Codigo INT, Nombre VARCHAR(100), PRIMARY KEY (Codigo)) ";
			String Query1 = "CREATE TABLE INVESTIGADORES (DNI VARCHAR(8), NomApels VARCHAR(255), Facultad INT, PRIMARY KEY (Id), FOREIGN KEY (Facultad) REFERENCES FACULTAD (Codigo)) ";
			String Query2 = "CREATE TABLE EQUIPOS (NumSerie Char(4), Nombre VARCHAR(100), Facultad INT, PRIMARY KEY (Id), FOREIGN KEY (Facultad) REFERENCES FACULTAD (Codigo)) ";
			String Query3 = "CREATE TABLE RESERVA (DNI VARCHAR , NumSerie char(4), Comienzo DATE, Fin DATE, PRIMARY KEY(DNI, NumSerie), FOREIGN KEY (DNI) REFERENCES INVESTIGADORES (DNI), FOREIGN KEY (NumSerie) REFERENCES EQUIPOS (NumSerie))";
			Statement st= conexion.createStatement();
			st.executeUpdate(Query);
			st.executeUpdate(Query1);
			st.executeUpdate(Query2);
			st.executeUpdate(Query3);
			System.out.println("Tablas creada con �xito");
			
		}
		catch (SQLException e){
			System.out.println(e.getMessage());
			System.out.println("Error al crear la tabla");
		}
		
	}
	
	//=====================================================================================
	
	public static void insertData() {
		
		try {
			
			String Querydb = "USE EJ09; ";
			Statement stdb= conexion.createStatement();
			stdb.executeUpdate(Querydb);
			

			String Query = "INSERT INTO FACULTAD VALUES (1,  \"Oxford\"), (2,  \"Wahington\"), ( 3,  \"Harvard\"), ( 4,  \"London\"), ( 5,  \"Springfield\");";
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Datos almacenados correctamente");
		
			String Query1 = "INSERT INTO INVESTIGADORES VALUES (35124865, \"Mario\", \"Oxford\"), (16425748, \"Josep\", \"Wahington\"), (12345124, \"jose\", \"Harvard\"), (22014523, \"Paco\", \"London\"), (99682451, \"Mat\", \"Springfield\");";
			Statement st1 = conexion.createStatement();
			st1.executeUpdate(Query1);
			System.out.println("Datos almacenados correctamente");
			
			String Query2 = "INSERT INTO EQUIPOS VALUES (0001, \"Mijos\", \"Oxford\"), (0002, \"Joses\", \"Wahington\"), (0003, \"Barcos\", \"Harvard\"), (0004, \"Bobos\", \"London\"), (0005, \"Nuts\", \"Springfield\");";
			Statement st2 = conexion.createStatement();
			st2.executeUpdate(Query2);
			System.out.println("Datos almacenados correctamente");
			
			String Query3 = "INSERT INTO RESERVA VALUES (35124865, 0001, 2020-06-14, 2020-06-15), (16425748, 0002, 2020-03-10, 2020-04-20), (12345124, 0003, 2020-01-20, 2020-03-20), (22014523, 0004, 2019-03-16, 2020-04-18), (99682451, 0005, 2018-03-20, 2020-08-5);";
			Statement st3 = conexion.createStatement();
			st3.executeUpdate(Query3);
			System.out.println("Datos almacenados correctamente");
			
		}
		catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("Error al insertar datos");
		}
		
	}
	
	//=====================================================================================

	public static void getValues() {
		

			try {
				
				String Query = "USE EJ09; ";
				Statement st= conexion.createStatement();
				st.executeUpdate(Query);
				
				String Query1 = "SELECT * FROM FACULTAD";
				Statement st1 = conexion.createStatement();
				java.sql.ResultSet resultSet1;
				resultSet1 = st1.executeQuery(Query1);
				
				while(resultSet1.next()) { 
					System.out.println("Codigo: " + resultSet1.getString("Codigo") + " Nombre: " + resultSet1.getString("Nombre"));
				}

				String Query2 = "SELECT * FROM INVESTIGADORES";
				Statement st2 = conexion.createStatement();
				java.sql.ResultSet resultSet2;
				resultSet2 = st2.executeQuery(Query2);
				
				while(resultSet2.next()) { 
					System.out.println("DNI: " + resultSet2.getString("DNI") + " NomApels: " + resultSet2.getString("NomApels") + " Facultad: " + resultSet2.getString("Facultad"));
				}
		
				String Query3 = "SELECT * FROM EQUIPOS";
				Statement st3 = conexion.createStatement();
				java.sql.ResultSet resultSet3;
				resultSet3 = st3.executeQuery(Query3);
				
				while(resultSet3.next()) { 
					System.out.println("NumSerie: " + resultSet3.getString("NumSerie") + " Nombre: " + resultSet3.getString("Nombre") + " Facultad: " + resultSet3.getString("Facultad"));
				}
				
				String Query4 = "SELECT * FROM RESERVA";
				Statement st4 = conexion.createStatement();
				java.sql.ResultSet resultSet4;
				resultSet4 = st4.executeQuery(Query4);
				
				while(resultSet4.next()) { 
					System.out.println("DNI: " + resultSet4.getString("DNI") + " NumSerie: " + resultSet4.getString("NumSerie") + " Comienzo: " + resultSet4.getString("Comienzo") + " Fin: " + resultSet4.getString("Fin"));
				}
				
			}
			catch (SQLException e) {
				System.out.println(e.getMessage());
				System.out.println("Error al mostrar datos");
			}	
		

	}
	
	
	//====================================================================================
	
	
	public static void deleteRecord() {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("De que tabla quieres eliminar?");
		String tabla = scan.next();
		
		if (tabla.equalsIgnoreCase("FACULTAD")) {
			
			System.out.println("Codigo que quieres eliminar");
			int numero = scan.nextInt();
		
			try {
				String Query = "DELETE FROM FACULTAD WHERE Codigo = "+ numero ; 
				Statement stDelete = conexion.createStatement();
				stDelete.executeUpdate(Query);
				System.out.println("Registro eliminado");
			}
			catch (SQLException e) {
				System.out.println(e.getMessage());
				System.out.println("Error al borrar el especificado");
			}
		}
		
		else if (tabla.equalsIgnoreCase("INVESTIGADORES")) {
			System.out.println("DNI Investigador que quieres eliminar");
			int numero = scan.nextInt();
		
			try {
				String Query = "DELETE FROM INVESTIGADORES WHERE DNI = " + numero; 
				Statement stDelete = conexion.createStatement();
				stDelete.executeUpdate(Query);
				System.out.println("Registro eliminado");
			}
			catch (SQLException e) {
				System.out.println(e.getMessage());
				System.out.println("Error al borrar el especificado");
			}
		}
		
		else if (tabla.equalsIgnoreCase("EQUIPOS")) {
			System.out.println("Codigo Pieza que quieres eliminar");
			int numero = scan.nextInt();
		
			try {
				String Query = "DELETE FROM EQUIPOS WHERE NumSerie = " + numero; 
				Statement stDelete = conexion.createStatement();
				stDelete.executeUpdate(Query);
				System.out.println("Registro eliminado");
			}
			catch (SQLException e) {
				System.out.println(e.getMessage());
				System.out.println("Error al borrar el especificado");
			}
		}
		
		else if (tabla.equalsIgnoreCase("RESERVA")) {
			System.out.println("NumSerie que quieres eliminar");
			int numero = scan.nextInt();
		
			try {
				String Query = "DELETE FROM RESERVA WHERE NumSerie = " + numero; 
				Statement stDelete = conexion.createStatement();
				stDelete.executeUpdate(Query);
				System.out.println("Registro eliminado");
			}
			catch (SQLException e) {
				System.out.println(e.getMessage());
				System.out.println("Error al borrar el especificado");
			}
		}
		
		else {
			System.out.println("Nombre de tabla incorrecto");
		}
		
		scan.close();

	}
	
	//=====================================================================================
	
	
	public static void closeConnection() {
		try {
			conexion.close();
			System.out.println("Conexion finalizada");
		}
		catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("Error al cerrar conexion");
		}
		
	}
	
}

