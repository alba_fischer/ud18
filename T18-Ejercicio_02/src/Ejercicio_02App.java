import dto.empleadosDB;
import dto.departamentosTable;
import dto.empleadosTable;

public class Ejercicio_02App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub	
		empleadosDB empleadosDB = new empleadosDB("192.168.1.24", "3306", "remote", "a7MWH!%p");
		empleadosDB.createDB();

		departamentosTable departamentos = new departamentosTable("192.168.1.24", "3306", "remote", "a7MWH!%p");
		departamentos.createTable();
		departamentos.insertData(001, "Tecnolog�a", 10000);
		departamentos.insertData(002, "Matem�ticas", 30000);
		departamentos.insertData(003, "F�sica", 15000);
		departamentos.insertData(004, "Biolog�a", 10000);
		departamentos.insertData(005, "Qu�mica", 30210);
		departamentos.getData();
		departamentos.deleteRecord(001);

		empleadosTable empleados = new empleadosTable("192.168.1.24", "3306", "remote", "a7MWH!%p");
		empleados.createTable();
		empleados.insertData("31309722", "Josefa", "Tudela", 004);
		empleados.insertData("78888100", "Cayetano", "Cayetano", 002);
		empleados.insertData("40264817", "Sandra", "Correa", 002);
		empleados.insertData("45469722", "Nazaret", "Rovira", 005);
		empleados.insertData("71523765", "Khalid", "Galiano", 003);
		empleados.getData();		
		empleados.deleteRecord("45469722");
	}

}
