package dto;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class departamentosTable extends empleadosDB {
	
	public departamentosTable() {
		super();
	}
	
	public departamentosTable(String ip, String port, String user, String password) {
		super(ip, port, user, password);
	}
	
	public void createTable() {
		Connection conexion = connectDB();
		
		try {
			String queryUse = "USE empleados;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String queryCreate = "CREATE TABLE DEPARTAMENTOS ("
					+ "codigo INT, "
					+ "nombre NVARCHAR(100),"
					+ "presupuesto INT,"
					+ "PRIMARY KEY(codigo));";
			Statement stCreate = conexion.createStatement();
			stCreate.executeUpdate(queryCreate);
			
			System.out.println("La tabla empleados ha sido creada con exito");
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al crear la tabla empleados: " + e);
		}
		
		disconnectDB(conexion);
	}
	
	public void insertData(int codigo, String nombre, int presupuesto) {
		Connection conexion = connectDB();
		
		try {
			String queryUse = "USE empleados;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String queryInsert = "INSERT INTO DEPARTAMENTOS (codigo, nombre, presupuesto) VALUES (" + codigo + ", " + "'" + nombre + "'" + ", " +  presupuesto + ");";
			Statement stInsert = conexion.createStatement();
			stInsert.executeUpdate(queryInsert);
			
			System.out.println("Los datos han sido introducidos con exito");
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al introducir los datos: " + e);
		}
		
		disconnectDB(conexion);
	}
	
	public void getData() {
		Connection conexion = connectDB();
		
		try {
			String queryUse = "USE empleados;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String querySelect = "SELECT * FROM DEPARTAMENTOS";
			Statement stSelect = conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = stSelect.executeQuery(querySelect);
			
			while(resultSet.next()) {
				System.out.println("Codigo: " + resultSet.getString("codigo") + " - "
				+ "Nombre: " + resultSet.getString("nombre") + " - "
				+ "Presupuesto: " + resultSet.getString("presupuesto")
				);
			}
			
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al obtener la informacion: " + e);
		}
		
		disconnectDB(conexion);
	}
	
	public void deleteRecord(int codigo) {
		Connection conexion = connectDB();
		
		try {
			String queryUse = "USE empleados;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String queryDelete = "DELETE FROM DEPARTAMENTOS WHERE codigo = " + codigo;
			Statement stDelete = conexion.createStatement();
			stDelete.executeUpdate(queryDelete);
			
			System.out.println("El registro ha sido eliminado correctamente");
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al eliminar el registro: " + e);
		}
		
		disconnectDB(conexion);
	}
}
