package dto;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class empleadosTable extends empleadosDB {
	
	public empleadosTable() {
		super();
	}
	
	public empleadosTable(String ip, String port, String user, String password) {
		super(ip, port, user, password);
	}
	
	public void createTable() {
		Connection conexion = connectDB();
		
		try {
			String queryUse = "USE empleados;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String queryCreate = "CREATE TABLE EMPLEADOS ("
					+ "dni VARCHAR(8), "
					+ "nombre NVARCHAR(100),"
					+ "apellidos NVARCHAR(255),"
					+ "departamento INT,"
					+ "PRIMARY KEY(dni),"
					+ "FOREIGN KEY (departamento) REFERENCES DEPARTAMENTOS(codigo));";
			Statement stCreate = conexion.createStatement();
			stCreate.executeUpdate(queryCreate);
			
			System.out.println("La tabla empleados ha sido creada con exito");
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al crear la tabla empleados: " + e);
		}
		
		disconnectDB(conexion);
	}
	
	public void insertData(String dni, String nombre, String apellidos, int departamento) {
		Connection conexion = connectDB();

		try {
			String queryUse = "USE empleados;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String queryInsert = "INSERT INTO EMPLEADOS (dni, nombre, apellidos, departamento) VALUES (" + "'" + dni + "'" + ", " + "'" + nombre + "'" + ", " + "'" + apellidos + "'" + ", " + departamento + ");";
			Statement stInsert = conexion.createStatement();
			stInsert.executeUpdate(queryInsert);
			
			System.out.println("Los datos han sido introducidos con exito");
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al introducir los datos: " + e);
		}
		
		disconnectDB(conexion);
	}
	
	public void getData() {
		Connection conexion = connectDB();

		try {
			String queryUse = "USE empleados;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String querySelect = "SELECT * FROM EMPLEADOS";
			Statement stSelect = conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = stSelect.executeQuery(querySelect);
			
			while(resultSet.next()) {
				System.out.println("DNI: " + resultSet.getString("dni") + " - "
				+ "Nombre: " + resultSet.getString("nombre") + " - "
				+ "Apellidos: " + resultSet.getString("apellidos") + " - "
				+ "Departamento: " + resultSet.getString("departamento")
				);
			}
			
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al obtener la informacion: " + e);
		}
		
		disconnectDB(conexion);
	}
	
	public void deleteRecord(String dni) {
		Connection conexion = connectDB();
		
		try {
			String queryUse = "USE empleados;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String queryDelete = "DELETE FROM EMPLEADOS WHERE dni = " + dni;
			Statement stDelete = conexion.createStatement();
			stDelete.executeUpdate(queryDelete);
			
			System.out.println("El registro ha sido eliminado correctamente");
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al eliminar el registro: " + e);
		}
		
		disconnectDB(conexion);
	}
	
}
