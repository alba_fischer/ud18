package conexiones;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class conexion {
			
	public static Connection conexion;

	public static void inicio() {
		openConnection();
		createDB();
		createTable();
		insertData();
		getValues();
		deleteRecord();
		closeConnection();
	}
	
	//=====================================================================================
	
	public static void openConnection() {
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			conexion=DriverManager.getConnection("jdbc:mysql://192.168.1.118:3306?useTimezone=true&serverTimezone=UTC", "remote", "Remote123***");
			System.out.println("Conexion completada con �xito");
		}
		catch (SQLException | ClassNotFoundException e) {
			System.out.println(e.getMessage());
			System.out.println("Error al conectar con base de datos");
		}
		
	}
	
	//=====================================================================================
	
	public static void createDB() {
		try {
			String Query = "CREATE DATABASE LOGISTICA;";
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Se ha creado la base de datos con �xito");
		}
		catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("Error al crear la base de datos");
		}
		
	}
	
	//=====================================================================================
	
	public static void createTable() {
		
		try {
			String Querydb = "USE LOGISTICA; ";
			Statement stdb= conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "CREATE TABLE ALMACENES (Codigo INT PRIMARY KEY, Lugar VARCHAR(100), Capacidad INT) ";
			String query = "CREATE TABLE CAJAS (NumReferencia char(5) , Contenido VARCHAR(100), Valor INT, Almacen INT, PRIMARY KEY (NumReferencia), FOREIGN KEY (Almacen) REFERENCES ALMACENES (Codigo))";
			Statement st= conexion.createStatement();
			st.executeUpdate(Query);
			st.executeUpdate(query);
			System.out.println("Tabla 1 creada con �xito");
			
			
		}
		catch (SQLException e){
			System.out.println(e.getMessage());
			System.out.println("Error al crear la tabla");
		}
		
	}
	
	//=====================================================================================
	
	public static void insertData() {
		
		try {
			
			String Querydb = "USE LOGISTICA; ";
			Statement stdb= conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			//insert into ALMACENES
			String Query = "INSERT INTO ALMACENES VALUES ( 1,  \"Reus\",  20), ( 2,  \"Tarragona\",  30), ( 3,  \"Valls\",  15), ( 4,  \"Valencia\",  80), ( 5,  \"Vic\",  200) ;";
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Datos almacenados correctamente");
		
			//insert into CAJAS
			String Query1 = "INSERT INTO CAJAS VALUES ( 00001,  \"Cocina\",  2600,  1), ( 00002,  \"Electronica\",  1400,  2), ( 00003,  \"Libros\",  340,  3), ( 00004, \"Herramientas\", 2400, 4), (00005, \"Alimento\", 150, 5);";
			Statement st1 = conexion.createStatement();
			st1.executeUpdate(Query1);
			System.out.println("Datos almacenados correctamente");
			
		}
		catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("Error al insertar datos");
		}
		
	}
	
	//=====================================================================================

	public static void getValues() {
		
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("De que tabla quieres mostrar?");
		String tabla = scan.next();
		if (tabla.equalsIgnoreCase("ALMACENES")) {
			try {
				
				String Querydb = "USE LOGISTICA; ";
				Statement stdb= conexion.createStatement();
				stdb.executeUpdate(Querydb);
				
				String Query = "SELECT * FROM ALMACENES";
				Statement st = conexion.createStatement();
				java.sql.ResultSet resultSet;
				resultSet = st.executeQuery(Query);
				
				while(resultSet.next()) { //codigo, lugar, capacidad
					System.out.println("Codigo: " + resultSet.getString("Codigo") + " Lugar: " + resultSet.getString("Lugar") + " Capacidad: " + resultSet.getString("Capacidad"));
				}
			
			}
			catch (SQLException e) {
				System.out.println(e.getMessage());
				System.out.println("Error al mostrar datos");
			}
		}else if (tabla.equalsIgnoreCase("CAJAS")) {
			try {
				
				String Querydb = "USE LOGISTICA; ";
				Statement stdb= conexion.createStatement();
				stdb.executeUpdate(Querydb);
				
				String Query = "SELECT * FROM CAJAS";
				Statement st = conexion.createStatement();
				java.sql.ResultSet resultSet;
				resultSet = st.executeQuery(Query);
				
				while(resultSet.next()) { //codigo, lugar, capacidad
					System.out.println("NumReferencia: " + resultSet.getString("NumReferencia") + " Contenido: " + resultSet.getString("Contenido") + " Valor: " + resultSet.getString("Valor") + " Almacen: " + resultSet.getString("Almacen"));
				}
			
			}
			catch (SQLException e) {
				System.out.println(e.getMessage());
				System.out.println("Error al mostrar datos");
			}
		}
		else {
			System.out.println("Nombre de tabla incorrecto");
		}
		
	}
	
	
	//=====================================================================================
	
	public static void deleteRecord() {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("De que tabla quieres eliminar?");
		String tabla = scan.next();
		
		if (tabla.equalsIgnoreCase("ALMACENES")) {
			
			System.out.println("Codigo que quieres eliminar");
			int numero = scan.nextInt();
		
			try {
				String Query = "DELETE FROM ALMACENES WHERE Codigo = "+ numero ; 
				Statement stDelete = conexion.createStatement();
				stDelete.executeUpdate(Query);
				System.out.println("Registro eliminado");
			}
			catch (SQLException e) {
				System.out.println(e.getMessage());
				System.out.println("Error al borrar el especificado");
			}
		}
		else if (tabla.equalsIgnoreCase("CAJAS")) {
			System.out.println("Numero Referencia que quieres eliminar");
			int numero = scan.nextInt();
		
			try {
				String Query = "DELETE FROM CAJAS WHERE NumReferencia = " + numero; 
				Statement stDelete = conexion.createStatement();
				stDelete.executeUpdate(Query);
				System.out.println("Registro eliminado");
			}
			catch (SQLException e) {
				System.out.println(e.getMessage());
				System.out.println("Error al borrar el especificado");
			}
		}
		else {
			System.out.println("Nombre de tabla incorrecto");
		}

	}
	
	//=====================================================================================
	
	
	public static void closeConnection() {
		try {
			conexion.close();
			System.out.println("Conexion finalizada");
		}
		catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("Error al cerrar conexion");
		}
		
	}
	
}

