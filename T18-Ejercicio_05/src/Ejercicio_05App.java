import dto.*;

public class Ejercicio_05App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub	
		directoresDB directoresDB = new directoresDB("192.168.1.24", "3306", "remote", "a7MWH!%p");
		directoresDB.createDB();

		despachosTable despachos = new despachosTable("192.168.1.24", "3306", "remote", "a7MWH!%p");
		despachos.createTable();
		despachos.insertData(001, 1);
		despachos.insertData(002, 5);
		despachos.insertData(003, 3);
		despachos.insertData(004, 6);
		despachos.insertData(005, 4);
		despachos.getData();
		despachos.deleteRecord(001);

		directoresTable directores = new directoresTable("192.168.1.24", "3306", "remote", "a7MWH!%p");
		directores.createTable();
		directores.insertData("31309722", "Josefa Tudela", "31309722", 003);
		directores.insertData("78888100", "Cayetano Cayetano", "31309722", 005);
		directores.insertData("40264817", "Sandra Correa", "78888100", 002);
		directores.insertData("45469722", "Nazaret Rovira", "78888100", 002);
		directores.insertData("71523765", "Khalid Galiano", "45469722", 004);
		directores.getData();		
		directores.deleteRecord("45469722");

	}

}
