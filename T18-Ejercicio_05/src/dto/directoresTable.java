package dto;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class directoresTable extends directoresDB {
	
	public directoresTable() {
		super();
	}
	
	public directoresTable(String ip, String port, String user, String password) {
		super(ip, port, user, password);
	}
	
	public void createTable() {
		Connection conexion = connectDB();
		
		try {
			String queryUse = "USE directores;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String queryCreate = "CREATE TABLE DIRECTORES ("
					+ "dni VARCHAR(8), "
					+ "nom_apels NVARCHAR(255),"
					+ "dni_jefe VARCHAR(8),"
					+ "despacho INT,"
					+ "PRIMARY KEY(dni),"
					+ "FOREIGN KEY (dni_jefe) REFERENCES DIRECTORES(dni),"
					+ "FOREIGN KEY (despacho) REFERENCES DESPACHOS(numero));";
			Statement stCreate = conexion.createStatement();
			stCreate.executeUpdate(queryCreate);
			
			System.out.println("La tabla directores ha sido creada con exito");
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al crear la tabla empleados: " + e);
		}
		
		disconnectDB(conexion);
	}
	
	public void insertData(String dni, String nom_apels, String dni_jefe, int despacho) {
		Connection conexion = connectDB();

		try {
			String queryUse = "USE directores;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String queryInsert = "INSERT INTO DIRECTORES (dni, nom_apels, dni_jefe, despacho) VALUES (" + "'" + dni + "'" + ", " + "'" + nom_apels + "'" + ", " + "'" + dni_jefe + "'" + ", " + despacho + ");";
			Statement stInsert = conexion.createStatement();
			stInsert.executeUpdate(queryInsert);
			
			System.out.println("Los datos han sido introducidos con exito");
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al introducir los datos: " + e);
		}
		
		disconnectDB(conexion);
	}
	
	public void getData() {
		Connection conexion = connectDB();

		try {
			String queryUse = "USE directores;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String querySelect = "SELECT * FROM DIRECTORES";
			Statement stSelect = conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = stSelect.executeQuery(querySelect);
			
			while(resultSet.next()) {
				System.out.println("DNI: " + resultSet.getString("dni") + " - "
				+ "Nombre y apellidos: " + resultSet.getString("nom_apels") + " - "
				+ "DNI Jefe: " + resultSet.getString("dni_jefe") + " - "
				+ "Despacho: " + resultSet.getString("despacho")
				);
			}
			
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al obtener la informacion: " + e);
		}
		
		disconnectDB(conexion);
	}
	
	public void deleteRecord(String dni) {
		Connection conexion = connectDB();
		
		try {
			String queryUse = "USE directores;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String queryDelete = "DELETE FROM DIRECTORES WHERE dni = " + dni;
			Statement stDelete = conexion.createStatement();
			stDelete.executeUpdate(queryDelete);
			
			System.out.println("El registro ha sido eliminado correctamente");
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al eliminar el registro: " + e);
		}
		
		disconnectDB(conexion);
	}
	
}