package dto;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class despachosTable extends directoresDB {
	
	public despachosTable() {
		super();
	}
	
	public despachosTable(String ip, String port, String user, String password) {
		super(ip, port, user, password);
	}
	
	public void createTable() {
		Connection conexion = connectDB();
		
		try {
			String queryUse = "USE directores;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String queryCreate = "CREATE TABLE DESPACHOS ("
					+ "numero INT, "
					+ "capacidad INT,"
					+ "PRIMARY KEY(numero));";
			Statement stCreate = conexion.createStatement();
			stCreate.executeUpdate(queryCreate);
			
			System.out.println("La tabla despachos ha sido creada con exito");
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al crear la tabla empleados: " + e);
		}
		
		disconnectDB(conexion);
	}
	
	public void insertData(int numero, int capacidad) {
		Connection conexion = connectDB();
		
		try {
			String queryUse = "USE directores;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String queryInsert = "INSERT INTO DESPACHOS (numero, capacidad) VALUES (" + numero + ", " + capacidad + ");";
			Statement stInsert = conexion.createStatement();
			stInsert.executeUpdate(queryInsert);
			
			System.out.println("Los datos han sido introducidos con exito");
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al introducir los datos: " + e);
		}
		
		disconnectDB(conexion);
	}
	
	public void getData() {
		Connection conexion = connectDB();
		
		try {
			String queryUse = "USE directores;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String querySelect = "SELECT * FROM DESPACHOS";
			Statement stSelect = conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = stSelect.executeQuery(querySelect);
			
			while(resultSet.next()) {
				System.out.println("Numero: " + resultSet.getString("numero") + " - "
				+ "Capacidad: " + resultSet.getString("capacidad")
				);
			}
			
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al obtener la informacion: " + e);
		}
		
		disconnectDB(conexion);
	}
	
	public void deleteRecord(int numero) {
		Connection conexion = connectDB();
		
		try {
			String queryUse = "USE directores;";
			Statement stUse = conexion.createStatement();
			stUse.executeUpdate(queryUse);
			
			String queryDelete = "DELETE FROM DESPACHOS WHERE numero = " + numero;
			Statement stDelete = conexion.createStatement();
			stDelete.executeUpdate(queryDelete);
			
			System.out.println("El registro ha sido eliminado correctamente");
		} catch (SQLException e) {
			System.out.println("Ha ocurrido un error al eliminar el registro: " + e);
		}
		
		disconnectDB(conexion);
	}
}
